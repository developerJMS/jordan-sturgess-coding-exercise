import employees.Employee;
import employees.SoftwareDeveloper;

public class PaySlipDriver {
    public static void main(String[] args) {

        /**
         * Valid employees to test payslip generation
         *  */
        Employee John = new SoftwareDeveloper("John","Smith","Junior Developer",
                10000,10);

        Employee Ara = new SoftwareDeveloper("Ara","Ayad","Junior Developer",
                30000,10);

        Employee Anisha = new SoftwareDeveloper("Anisha","Katri","Junior Developer",
                60000,10);

        Employee Son = new SoftwareDeveloper("Son","Vuong","Junior Developer",
                90000,10);

        Employee Karan = new SoftwareDeveloper("Karan","Bankole","Junior Developer",
                210000,10);

        PaySlip JohnFebruary = new PaySlip(John, "February");
        System.out.println(JohnFebruary);

        PaySlip AraMarch = new PaySlip(Ara, "March");
        System.out.println(AraMarch);

        PaySlip AnishaAugust = new PaySlip(Anisha, "August");
        System.out.println(AnishaAugust);

        PaySlip SonNovember = new PaySlip(Son, "November");
        System.out.println(SonNovember);

        PaySlip KaranDecember = new PaySlip(Karan, "December");
        System.out.println(KaranDecember);
    }
}
