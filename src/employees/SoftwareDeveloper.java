package employees;

import java.util.ArrayList;
import java.util.List;

public class SoftwareDeveloper extends Employee {
    /** Stores a list of possible job titles for this employee type */
    static final List<String> JOB_TITLES = new ArrayList<>(List.of("Junior Developer", "Senior Developer"));

    /**
     * Constructor
     * @param initJobTitle: employee's job title e.g. "Senior Developer"
     */
    public SoftwareDeveloper(String initFirstName, String initLastName, String initJobTitle, int initAnnualSalary,
                             double initSuperRate) {
        super(initFirstName, initLastName, initAnnualSalary, initSuperRate);

        setJobTitle(initJobTitle);
    }

    public void setJobTitle(String newJobTitle) {
        for (String i : JOB_TITLES) {
            if (newJobTitle.equalsIgnoreCase(i))
                jobTitle = newJobTitle;
        }
        if (jobTitle == null) {
            System.out.println("Invalid job title, please enter one of: " + JOB_TITLES.toString());
            setJobTitle(validate.nextLine());
        }
    }
}