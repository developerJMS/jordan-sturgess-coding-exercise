package employees;

public class EmployeeDriver {
    public static void main(String[] args) {

        /** Testing valid Software dev. employee */
        Employee John = new SoftwareDeveloper("John","Smith","Junior Developer",
                80000,30);
        System.out.println(John);

        /** Testing invalid Software dev. employee */
        Employee Ara = new SoftwareDeveloper("Ara","Ayad","Jenior Developer",
                -200,55);
        System.out.println(Ara);

        /** Testing valid HR employee */
        Employee Anisha = new HumanResources("Anisha","Khatri","Receptionist",
                65000,45);
        System.out.println(Anisha);

        /** Testing invalid HR employee */
        Employee Son = new HumanResources("Son","Vuong","Baker",
                0,-10);
        System.out.println(Son);
    }
}
