package employees;

import java.util.Scanner;

public abstract class Employee {
    Scanner validate = new Scanner(System.in);
    String jobTitle;

    String firstName;
    String lastName;

    int annualSalary;
    double superRate;

    /**
     * Constructor
     * @param initFirstName: employee's first name
     * @param initLastName: employee's last name
     * @param initAnnualSalary: annual salary > 0
     * @param initSuperRate: super rate, 0 - 50% inclusive
     */
    public Employee(String initFirstName, String initLastName, int initAnnualSalary,
                    double initSuperRate) {

        firstName = initFirstName;
        lastName = initLastName;

        setAnnualSalary(initAnnualSalary);
        setSuperRate(initSuperRate);
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAnnualSalary() {
        return annualSalary;
    }

    public double getSuperRate() {
        return superRate;
    }

    public void setFirstName(String newFirstName) {
        firstName = newFirstName;
    }

    public void setLastName(String newLastName) {
        lastName = newLastName;
    }

    /**
     *
     * @param newAnnualSalary: new employee salary to validate
     */
    public void setAnnualSalary(int newAnnualSalary) {
        if (newAnnualSalary > 0)
            annualSalary = newAnnualSalary;
        else {
            System.out.println("Salary must be a positive integer, please re-enter: ");
            setAnnualSalary(validate.nextInt());
        }
    }

    /**
     *
     * @param newSuperRate: new employee super rate to validate
     */
    public void setSuperRate(double newSuperRate) {
        if (newSuperRate >= 0 && newSuperRate <= 50)
            superRate = newSuperRate/100;
        else {
            System.out.println("Super must be between 0 and 50 inclusive, please re-enter: ");
            setSuperRate(validate.nextInt());
        }
    }

    public String toString() {
        String employeeInfo = "";

        employeeInfo += this.getFirstName() + " ";
        employeeInfo += this.getLastName() + ", ";
        employeeInfo += this.getJobTitle() + ", ";
        employeeInfo += "$" + this.getAnnualSalary() + ", ";
        employeeInfo += this.getSuperRate() + "%, ";

        return employeeInfo;
    }
}