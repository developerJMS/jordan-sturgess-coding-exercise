import employees.Employee;

import java.time.LocalDate;

public class PaySlip {
    String employeeName;

    String employeePayPeriod;
    int employeeNetIncome;
    int employeeGrossIncome;
    int employeeIncomeTax;
    int employeeSuper;

    /**
     * Constructor
     * @param initEmployee: reference to a valid employee
     * @param initPayPeriod: the month for the payslip
     */
    public PaySlip(Employee initEmployee,String initPayPeriod) {
        int employeeSalary = initEmployee.getAnnualSalary();

        employeeName = initEmployee.getFirstName() + " " + initEmployee.getLastName();

        employeePayPeriod = payPeriodGenerator(initPayPeriod);
        employeeGrossIncome = (int) Math.floor(employeeSalary/12);
        employeeIncomeTax = incomeTaxCalculator(employeeSalary);
        employeeNetIncome = employeeGrossIncome - employeeIncomeTax;
        employeeSuper = (int) Math.floor(employeeGrossIncome * initEmployee.getSuperRate());
    }

    /**
     * *
     * @param newPayPeriod: the month for the payslip
     * @return a string denoting the pay period
     */
    private String payPeriodGenerator(String newPayPeriod) {
        String payPeriod = "01 ";

        switch (newPayPeriod) {
            case "January":
                payPeriod += "January - 31 January";
                break;
            case "February":
                boolean leapYear = checkLeapYear(LocalDate.now().getYear());
                if (leapYear)
                    payPeriod += "February - 29 February";
                else
                    payPeriod += "February - 28 February";
                break;
            case "March":
                payPeriod += "March - 31 March";
                break;
            case "April":
                payPeriod += "April - 30 April";
                break;
            case "May":
                payPeriod += "May - 31 May";
                break;
            case "June":
                payPeriod += "June - 30 June";
                break;
            case "July":
                payPeriod += "July - 31 July";
                break;
            case "August":
                payPeriod += "August - 31 August";
                break;
            case "September":
                payPeriod += "September - 30 September";
                break;
            case "October":
                payPeriod += "October - 31 October";
                break;
            case "November":
                payPeriod += "November - 30 November";
                break;
            case "December":
                payPeriod += "December - 31 December";
                break;
            default:
                payPeriod += "Error";
        }

        return payPeriod;
    }

    /**
     * *
     * @param yearToCheck: the current year
     * @return true if year the current year is a leap year
     */
    private boolean checkLeapYear(int yearToCheck) {
        boolean isLeapYear;

            if (yearToCheck % 4 == 0) {
                if (yearToCheck % 100 == 0) {
                    if (yearToCheck % 400 == 0)
                        isLeapYear = true;
                    else
                        isLeapYear = false;
                } else
                    isLeapYear = true;
            } else
                isLeapYear = false;

        return isLeapYear;
    }

    /**
     *
     * @param salary: an employee's annual salary
     * @return income tax an employee pays each month
     */
    private int incomeTaxCalculator(int salary) {
        int incomeTax;

        if (salary <= 18200) {
            incomeTax = 0;
        }
        else if (salary >= 18201 && salary <= 37000) {
            incomeTax = (int) Math.round(((salary - 18200) * 0.19) / 12);
        }
        else if (salary >= 37001 && salary <= 87000) {
            incomeTax = (int) Math.round((3572 + (salary - 37000) * 0.325) / 12);
        }
        else if (salary >= 87001 && salary <= 180000) {
            incomeTax = (int) Math.round((19822 + (salary - 87000) * 0.37) / 12);
        }
        else {
            incomeTax = (int) Math.round((54232 + (salary - 180000) * 0.45) / 12);
        }

        return incomeTax;
    }

    public String toString() {
        String paySlipInfo = "";

        paySlipInfo += this.employeeName + ", ";
        paySlipInfo += this.employeePayPeriod + ", ";
        paySlipInfo += "$" + this.employeeGrossIncome + ", ";
        paySlipInfo += "$" + this.employeeIncomeTax + ", ";
        paySlipInfo += "$" + this.employeeNetIncome + ", ";
        paySlipInfo += "$" + this.employeeSuper;

        return paySlipInfo;
    }
}
